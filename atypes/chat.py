from .base import TelegramObject
from .fields import Field


class Chat(TelegramObject):
    id = Field()
    index = Field(alias='id')
    title = Field()
