import datetime

from .base import TelegramObject
from .chat import Chat
from .fields import DateTimeField, Field
from .user import User


class Message(TelegramObject):
    id: int = Field()
    index: int = Field(alias='id')
    from_user: 'User' = Field(alias='from', base='User')
    chat: Chat = Field(base='Chat')
    date: datetime.datetime = DateTimeField()
