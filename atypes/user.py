from .base import TelegramObject
from .fields import Field


class User(TelegramObject):
    id: int = Field()
    index: int = Field(alias='id')
    first_name: str = Field()
    last_name: str = Field()
