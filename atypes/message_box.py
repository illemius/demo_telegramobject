import typing

from .base import TelegramObject
from .fields import Field, ListField
from .message import Message


class MessagesBox(TelegramObject):
    id: int = Field()
    index: int = Field(alias='id')
    messages: typing.List[Message] = ListField(base=Message)
