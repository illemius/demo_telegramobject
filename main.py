from atypes.message import Message

MSG = {
    'id': 42,
    'from': {
        'id': 1337228,
        'first_name': 'Foo',
        'last_name': 'Bar'
    },
    'chat': {
        'id': -1234567890,
    },
    'date': 1500000000
}

message = Message(**MSG)

# Stringify message and print
print(message)
# {"id":42,"from":{"id":1337228,"first_name":"Foo","last_name":"Bar"},"chat":{"id":-123456789000},"date":1500000000}

# Get property 'from_user' (in original object is 'from')
print(message.from_user)
# {"id":1337228,"first_name":"Foo","last_name":"Bar"}

# Can be duplicated fields (Different properties can be linked to one field)
print(message.from_user.index)
# 1337228

# Timestamp as datetime
print(message.date)
# 2017-07-14 05:40:00

# Get chat as Python dict
print(message.chat.to_python())
# {'id': -123456789000}
